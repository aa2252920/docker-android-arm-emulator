#!/bin/sh -eu
# Run within container with volume mounted

/opt/android-sdk/cmdline-tools/tools/bin/sdkmanager --update
/opt/android-sdk/tools/bin/sdkmanager "emulator" "platform-tools" "platforms;android-25" "system-images;android-25;google_apis;armeabi-v7a"
